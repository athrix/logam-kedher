BACKGROUND

This Organelle patch attempts to replicate the sounds of some of the
Balinese gong instruments. (The name of the patch, "Logam Kedher,"
means "metal vibrates" in Javanese.)

In writing it, I have used information from a research paper by Lydia
Ayers and Andrew Horner, as of this writing available on the web at
http://www.music.mcgill.ca/~ich/research/misc/papers/cr1116.pdf. Although
that paper includes code, I have not replicated their exact
formulation. Primarily I have used the number and frequency of the
harmonics which they describe from their analysis of the instrument
known as "gong ageng."  There are nine harmonics (including the base),
each of which is subjected once or twice to two kinds of modulation,
with different parameters. (See the original paper for more
information, or read the code.)

This version of the patch is monophonic. If the OrganelleM can handle
it, I'll probably add polyphony to a subsequent version.


HOW TO USE

Turning the encoder switches between three pages, the third of which
is only used to return to the Organelle's menu (by pressing the encoder).
The first two pages control the following parameters:

Octave: Specifies the octave of the built-in keyboard, on a range from
-4 to +4 inclusive. Note that things get a little weird at the high
couple octaves and it's not clear that these are useful musically in
the same way as the lower octaves.

Frequency variation: Allows a random positive or negative amount to be
added to the frequency of each harmonic as each note is struck. The
amount is specified in cents.

Amplitude variation: Allows a random positive or negative change in
the amplitude of each harmonic as each note is struck. The amount is
specified as a percentage of frequency.

Harmonic (amplitude) randomization: Allows (pseudo)randomization of
the amplitude of each harmonic, apart from the base note. This value
is constrained to be between 0 and 1000, with 0 representing the
default levels as stored in the patch. Per-harmonic amplitude
variation (see above) is still applied on each note-on event.

Attack: Controls note attack time, specified in millisenconds.

Release: Controls note release time, specified in milliseconds.

Reverb: Controls reverb wet/dry percentage. This is implemented via
freeverb, which is included.

If the Organelle's "Save" or "Save New" menu is chosen, the patch
should save all parameter values including any randomized harmonic
amplitudes in effect at the time of saving. If you wish more control,
examine the saved values in the file "state_values.txt" which is, if
you read the Ayers and Horner paper (and a bit of the patch code),
reasonably self-documenting.

Enjoy!

Jeremy Bornstein

P.S. The author gratefully acknowledges the assistance of Dan
Bornstein <danfuzz@milk.com> in many matters related to the production
of this patch, including the use of his "polite-clip~.pd" herein.
